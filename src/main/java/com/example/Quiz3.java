package com.example;

import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.client.annotation.Client;
import io.micronaut.reactor.http.client.ReactorHttpClient;
import jakarta.inject.Inject;

@Controller("quiz3")
public class Quiz3 {
  @Inject
  @Client("http://www.google.com")
  ReactorHttpClient googleClient;

  // http://localhost:8080/quiz3
  @Get
  public int googleStatus() {
    return googleClient.exchange("/")
        .block()
        .code();
  }
}
