package com.example;

import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@Controller("quiz1")
public class Quiz1 {
  Logger log = LoggerFactory.getLogger(Quiz1.class);

  @Get
  public String perform() {
    String hello = "Hello micronaut!";
    log.info("{}", hello);
    return hello;
  }

}
