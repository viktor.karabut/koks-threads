package com.example;

import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.client.HttpClient;
import io.micronaut.http.client.annotation.Client;
import jakarta.inject.Inject;
import reactor.core.publisher.Mono;

@Controller("quiz3a")
public class Quiz3a {
  @Inject
  @Client("http://www.google.com")
  HttpClient googleClient;

  @Get
  public Mono<Integer> googleStatus() {
    return Mono.from(googleClient.exchange("/"))
        .map(HttpResponse::code);
  }
}
