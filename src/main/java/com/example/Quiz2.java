package com.example;

import io.micronaut.core.annotation.Blocking;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.scheduling.TaskExecutors;
import io.micronaut.scheduling.annotation.ExecuteOn;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Controller("quiz2")
public class Quiz2 {
  private static final Logger log = LoggerFactory.getLogger(Quiz2.class);

  @Get("/a")
  @Blocking
  public String a() {
    log.info("Hello a!");
    return "ok";
  }

  @Get("/b")
  @ExecuteOn(TaskExecutors.IO)
  public String b() {
    log.info("Hello b!");
    return "ok";
  }

}
