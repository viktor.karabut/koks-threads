package com.example;

import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.client.HttpClient;
import io.micronaut.http.client.annotation.Client;
import io.micronaut.reactor.http.client.ReactorHttpClient;
import jakarta.inject.Inject;
import reactor.core.publisher.Mono;

@Controller("quiz4a")
public class Quiz4 {
  @Inject
  @Client("http://www.google.com")
  ReactorHttpClient googleClient;

  @Get
  public Mono<Integer> googleStatus() {
    return googleClient.exchange("/")
        .map(HttpResponse::code);
  }
}
