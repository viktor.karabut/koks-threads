package com.example;

import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;


@Controller("quiz1demo")
public class Quiz1Demo {
  Logger log = LoggerFactory.getLogger(Quiz1Demo.class);

  // http://localhost:8080/quiz1demo/fast
  @Get("/fast")
  public String fast() {
    log.info("Fast request!");
    return "fast";
  }

  // http://localhost:8080/quiz1demo/slow
  @Get("/slow")
  public String slow() throws Exception {
    log.info("Slow request!");
    Thread.sleep(TimeUnit.MINUTES.toMillis(1));
    return "slow";
  }

}
